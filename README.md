> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Erin Cowan	

### Assignment 3


#### README.md file should include the following items:

* a3.mwb file
* a3.sql file
* screen shot of ERD

###Deliverables:
1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
4. Blackboard Links: a3 bitbucket repo	
4. Provide Bitbucket read-only access to a3 repo must include README.md, using Markdown syntax, and include links to all of the following files (from README.md):
 
  - docs folder: a3.mwb, and a3.sql
  - img folder: a3.png (export a3.mwb file as a3.png)
  - README.md (MUST display a3.png ERD)




### Links:


[a3.mwb](docs/a3.mwb/ "a3.mwb")


[a3.sql](docs/a3.sql/ "a3.sql")






*Screenshot of ERD*:

![ERD screenshot](img/a3.png)

